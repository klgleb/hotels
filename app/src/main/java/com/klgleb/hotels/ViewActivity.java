package com.klgleb.hotels;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.klgleb.hotels.db.HelperFactory;
import com.klgleb.hotels.db.HotelDetailsDao;
import com.klgleb.hotels.rest.GetHotelDetailsTask;
import com.klgleb.hotels.rest.HotelDetails;
import com.klgleb.hotels.rest.ITaskCallback;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.androidannotations.annotations.AfterExtras;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.NonConfigurationInstance;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringRes;

import java.sql.SQLException;
import java.text.NumberFormat;
import java.text.ParsePosition;

/**
 * Activity for display hotel details
 * <p/>
 * Created by klgleb on 02.11.15.
 */

@EActivity(R.layout.activity_view)
@OptionsMenu(R.menu.menu_view)
public class ViewActivity extends AppCompatActivity implements ITaskCallback<HotelDetails> {

    public static final String TAG = "ViewActivity";
    public static final String DIALOG_TAG = "MyDialog";


    @StringRes(R.string.loading_waiting)
    String loadingPleaseWait;


    @StringRes(R.string.server_url)
    String serverUrl;

    @ViewById
    Toolbar toolbar;

    @ViewById(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;

    @ViewById
    ImageView imageView;

    @ViewById
    RatingBar ratingBar;

    @ViewById
    TextView addressText;

    @ViewById
    TextView suitesAvailabilityText;

    @ViewById
    TextView distanceText;

    @ViewById
    FloatingActionButton fab;


    @Extra
    Integer hotelId;


    @NonConfigurationInstance
    @Bean
    GetHotelDetailsTask getHotelDetailsTask;

    private HotelDetails currentHotel;
    private boolean imageFailed = false;


    @AfterViews
    void afterViews() {

        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
//            actionBar.setHomeButtonEnabled(true);
        }

//        setRefreshing(true);

        collapsingToolbar.setTitle("");
    }

    @AfterExtras
    void afterExtras() {
        getHotelDetailsTask.prepare(hotelId);
//        loadFromCache();

        if (getHotelDetailsTask.isLoading()) {
            setRefreshing(true);
        } else {
            setRefreshing(false);
            loadFromCache();
        }
    }

    @Background
    void loadFromCache() {

        HotelDetails hotelDetails = null;

        try {
            HotelDetailsDao hotelDao = HelperFactory.getHelper().getHotelDetailsDao();
            hotelDetails = hotelDao.getHotelById(hotelId);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (hotelDetails != null) {
            displayData(hotelDetails);
        } else {
            getHotelDetailsTask.doRequest();
        }

    }


    @OptionsItem(android.R.id.home)
    void onClickHomeButton() {
        finish();
    }

    @OptionsItem(R.id.action_update)
    void refrechInfo() {
        if (!getHotelDetailsTask.isLoading()) {
            getHotelDetailsTask.prepare(hotelId);
            getHotelDetailsTask.doRequest();
        }
    }

    @Click
    void fab() {
        Uri gmmIntentUri = Uri.parse(
                String.format("geo:%s,%s?q=%s", currentHotel.getLat(), currentHotel.getLon(),
                        currentHotel.getName()));
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
//        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        }

    }

    @Click(R.id.imageView)
    void showImage() {
        String imageUrl = getImageUrl();

        if (imageUrl == null || imageFailed) {
            return;
        }

        FullscreenActivity_.intent(this)
                .title(currentHotel.getName())
                .imageUrl(imageUrl)
                .start();
    }


    @Override
    @UiThread
    public void setRefreshing(boolean refreshing) {
        if (refreshing) {
            setRefreshing(false);
            new ProgressDialogFragment().show(getFragmentManager(), DIALOG_TAG);
        } else {
            ProgressDialogFragment fragmentByTag = (ProgressDialogFragment) getFragmentManager().findFragmentByTag(DIALOG_TAG);
            if (fragmentByTag != null) {
                fragmentByTag.getDialog().dismiss();
            }
        }
    }

    @Override
    @UiThread
    public void onError(String s) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();

        finish();//просто закрываем activity в случае ошибки.
    }

    @Override
    @UiThread
    public void displayData(HotelDetails hotel) {
        this.currentHotel = hotel;
        this.imageFailed = false;

        collapsingToolbar.setTitle(hotel.getName());

        ratingBar.setVisibility(View.VISIBLE);
        ratingBar.setRating((float) hotel.getStars());

        addressText.setText(hotel.getAddress());


        String suitesAvMsg = String.format(getString(R.string.suites_available),
                hotel.getSuitesAvailability().length);

        suitesAvailabilityText.setText(suitesAvMsg);


        distanceText.setText(String.format(getString(R.string.distance), 24));


        if (hotel.getImage() == null) {
            Log.w(TAG, "Image = null for this object");
            Log.w(TAG, String.valueOf(hotel.getId()));
            Log.w(TAG, String.valueOf(hotel.getName()));
            Log.w(TAG, String.valueOf(hotel.getImage()));
            return;
        }

        String url = getImageUrl();

        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .resetViewBeforeLoading()
                .cacheInMemory()
                .cacheOnDisc()
                .build();


        Log.d(TAG, "---loading---");
        Log.d(TAG, String.valueOf(hotel.getId()));
        Log.d(TAG, String.valueOf(hotel.getName()));
        Log.d(TAG, String.valueOf(hotel.getImage()));

        ImageLoader.getInstance().displayImage(url, imageView, options, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {

            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                imageFailed = true;
                Log.w(TAG, "Image  loading failed");
                Log.w(TAG, imageUri);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        });


    }

    private String getImageUrl() {
        String url = serverUrl + "/" + currentHotel.getImage();

        if (currentHotel.getImage() == null) {
            return null;
        }

        if (isNumeric(currentHotel.getImage())) {
            //Вообще-то, в задании написано о том, что расширение нужно приписывать всегда,
            //но по факту ответ в большинстве приходит уже с расширением.
            //Поэтому просто проверяем, id это или нет, и если да, то дополняем его расширением.
            url = url + ".jpg";
        }

        return url;
    }

    public static boolean isNumeric(String str) {
        if (str == null) return false;
        NumberFormat formatter = NumberFormat.getInstance();
        ParsePosition pos = new ParsePosition(0);
        formatter.parse(str, pos);
        return str.length() == pos.getIndex();
    }

    @Override
    protected void onDestroy() {
        setRefreshing(false);
        super.onDestroy();
    }
}
