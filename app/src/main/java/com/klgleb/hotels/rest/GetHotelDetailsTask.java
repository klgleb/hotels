package com.klgleb.hotels.rest;

import com.klgleb.hotels.R;
import com.klgleb.hotels.db.HelperFactory;
import com.klgleb.hotels.db.HotelDetailsDao;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.res.StringRes;

import java.sql.SQLException;

import retrofit.RestAdapter;

/**
 * Getting a list of hotels from server
 *
 * Created by klgleb on 02.11.15.
 */
@EBean
public class GetHotelDetailsTask extends BaseTask<HotelDetails> {

    @StringRes(R.string.server_url)
    String serverUrl;

    private int hotelId;

    public void prepare(int hotelId) {
        this.hotelId = hotelId;
    }

    @Override
    HotelDetails getData() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(serverUrl)
                .build();

        MyApiInterface api = restAdapter.create(MyApiInterface.class);

        return api.getHotelDetails(hotelId);
    }

    @Override
    public void cacheData(HotelDetails hotel) {
        try {
            HotelDetailsDao hotelDao = HelperFactory.getHelper().getHotelDetailsDao();
            hotelDao.cacheHotel(hotel);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
