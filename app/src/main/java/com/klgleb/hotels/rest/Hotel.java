package com.klgleb.hotels.rest;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Hotel model
 *
 * Created by klgleb on 02.11.15.
 */
@DatabaseTable(tableName = "hotels")
public class Hotel {

    @DatabaseField(id = true)
    int id;

    @DatabaseField
    String name;

    @DatabaseField
    String address;

    @DatabaseField
    double stars;

    @DatabaseField
    double distance;

    @DatabaseField
    String suites_availability;



    public Hotel() {

    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public double getStars() {
        return stars;
    }

    public double getDistance() {
        return distance;
    }

    public int[] getSuitesAvailability() {
        //TODO: сделать проверку возвращаемых сервером значений для поля suites_availability
        String[] strArray = suites_availability.split(":");

        int[] intArray = new int[strArray.length];
        for(int i = 0; i < strArray.length; i++) {
            intArray[i] = Integer.parseInt(strArray[i]);
        }


        return intArray;
    }

}
