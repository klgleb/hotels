package com.klgleb.hotels.rest;

import java.util.List;

import retrofit.http.GET;
import retrofit.http.Path;


public interface MyApiInterface {

    @GET("/0777.json")
    List<Hotel> getHotels();


    @GET("/{hotelId}.json")
    HotelDetails getHotelDetails(@Path("hotelId")int hotelId);


}
