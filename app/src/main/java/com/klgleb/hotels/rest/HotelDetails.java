package com.klgleb.hotels.rest;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Model of hotel details
 *
 * Created by klgleb on 02.11.15.
 */

@DatabaseTable(tableName = "hotel_details")
public class HotelDetails extends Hotel {

    @DatabaseField
    String image;

    @DatabaseField
    double lat;

    @DatabaseField
    double lon;


    public HotelDetails() {
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }
}
