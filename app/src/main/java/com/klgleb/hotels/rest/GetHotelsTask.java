package com.klgleb.hotels.rest;

import com.klgleb.hotels.R;
import com.klgleb.hotels.db.HelperFactory;
import com.klgleb.hotels.db.HotelDao;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.res.StringRes;

import java.sql.SQLException;
import java.util.List;

import retrofit.RestAdapter;

/**
 * Getting a list of hotels from server
 *
 * Created by klgleb on 02.11.15.
 */
@EBean
public class GetHotelsTask extends BaseTask<List<Hotel>> {

    @StringRes(R.string.server_url)
    String serverUrl;

    @Override
    public List<Hotel> getData() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(serverUrl)
                .build();

        MyApiInterface api = restAdapter.create(MyApiInterface.class);

        return api.getHotels();
    }

    @Override
    public void cacheData(List<Hotel> hotels) {
        try {
            HotelDao hotelDao = HelperFactory.getHelper().getHotelDao();
            hotelDao.setAllHotels(hotels);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
