package com.klgleb.hotels.db;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.klgleb.hotels.rest.Hotel;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

/**
 * ORMLite Hotel Dao
 * <p/>
 * Created by klgleb on 02.11.15.
 */
public class HotelDao extends BaseDaoImpl<Hotel, Integer> {
    protected HotelDao(ConnectionSource connectionSource,
                       Class<Hotel> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<Hotel> getAllHotels() throws SQLException {
        return this.queryForAll();
    }

    public void setAllHotels(List<Hotel> hotels) throws SQLException {
        TableUtils.clearTable(getConnectionSource(), Hotel.class);
        for (int i = 0; i < hotels.size(); i++) {
            createOrUpdate(hotels.get(i));
        }
    }
}
