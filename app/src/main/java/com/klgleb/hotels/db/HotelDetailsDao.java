package com.klgleb.hotels.db;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.klgleb.hotels.rest.HotelDetails;

import java.sql.SQLException;

/**
 * ORMLite HotelDetails Dao
 *
 * Created by klgleb on 02.11.15.
 */
public class HotelDetailsDao extends BaseDaoImpl<HotelDetails, Integer> {
    protected HotelDetailsDao(ConnectionSource connectionSource,
                       Class<HotelDetails> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public HotelDetails getHotelById(int id)  throws SQLException {
        return this.queryForId(id);
    }

    public void cacheHotel(HotelDetails hotel) throws SQLException {
        createOrUpdate(hotel);

    }
}
