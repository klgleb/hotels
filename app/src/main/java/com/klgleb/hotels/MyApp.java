package com.klgleb.hotels;

import android.app.Application;

import com.klgleb.hotels.db.HelperFactory;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

/**
 * Our application
 * <p/>
 * Created by klgleb on 02.11.15.
 */
public class MyApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(getApplicationContext()));

        HelperFactory.setHelper(getApplicationContext());
    }

    @Override
    public void onTerminate() {
        HelperFactory.releaseHelper();
        super.onTerminate();
    }
}
