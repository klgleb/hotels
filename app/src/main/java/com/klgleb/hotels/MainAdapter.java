package com.klgleb.hotels;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.klgleb.hotels.rest.Hotel;

import java.util.ArrayList;

/**
 * Adapter for listView in MainActivity
 * <p/>
 * Created by klgleb on 02.11.15.
 */
public class MainAdapter extends ArrayAdapter<Hotel> {
    public static final String TAG = "MainAdapter";

    public MainAdapter(Context context, ArrayList<Hotel> hotels) {
        super(context, R.layout.main_item, hotels);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        MyViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.main_item, parent, false);
            holder = new MyViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (MyViewHolder) convertView.getTag();
        }

        Hotel hotel = getItem(position);

        try {

            holder.nameText.setText(hotel.getName());

            String addressMsg = String.format(getContext().getString(R.string.address_and_distance),
                    hotel.getAddress(), hotel.getDistance());

            holder.addressText.setText(addressMsg);

            String suitesAvMsg = String.format(getContext().getString(R.string.suites_available),
                    hotel.getSuitesAvailability().length);

            holder.suitesAvailabilityText.setText(suitesAvMsg);

            float stars = (float) hotel.getStars();
            holder.ratingBar.setStepSize((float) 0.1);
            holder.ratingBar.setRating(stars);

        } catch (Throwable throwable) {
            Log.w(TAG, throwable);
        }

        return convertView;

    }

    private class MyViewHolder {
        TextView nameText, addressText, suitesAvailabilityText;
        RatingBar ratingBar;


        public MyViewHolder(View item) {
            nameText = (TextView) item.findViewById(R.id.nameText);
            addressText = (TextView) item.findViewById(R.id.addressText);
            suitesAvailabilityText = (TextView) item.findViewById(R.id.suitesAvailabilityText);
            ratingBar = (RatingBar) item.findViewById(R.id.ratingBar);
        }
    }
}
