package com.klgleb.hotels.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * ImageView without 1px border.
 *
 * Created by klgleb on 05.11.15.
 */
public class NoBorderImageView extends ImageView {
    public NoBorderImageView(Context context) {
        super(context);
    }

    public NoBorderImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NoBorderImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public NoBorderImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }


    @Override
    public void setImageBitmap(Bitmap bm) {
        super.setImageBitmap(new NoBorderHelper().doNoBorder(bm));
    }
}
