package com.klgleb.hotels.views;

import android.graphics.Bitmap;
import android.util.Log;

/**
 * Helper for NoBorder ImageViews, deleting 1px border.
 *
 * Created by klgleb on 05.11.15.
 */
public class NoBorderHelper {
    public static final String TAG = "NoBorderHelper";


    public Bitmap doNoBorder(Bitmap bm) {
        Bitmap bmp;

        try {
            bmp = Bitmap.createBitmap(bm, 1, 1, bm.getWidth() - 2, bm.getHeight() - 2);
        } catch (Throwable throwable){
            bmp = bm;
            Log.w(TAG, throwable);
        }

        return bmp;
    }
}
