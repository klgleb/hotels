package com.klgleb.hotels.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;

import com.ortiz.touch.TouchImageView;

/**
 * TouchImageView without 1px border.
 *
 * Created by klgleb on 05.11.15.
 */
public class NoBorderTouchImageView extends TouchImageView {

    public static final String TAG = "NoBorderTouchImageView";

    public NoBorderTouchImageView(Context context) {
        super(context);
    }

    public NoBorderTouchImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NoBorderTouchImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void setImageBitmap(Bitmap bm) {
        super.setImageBitmap(new NoBorderHelper().doNoBorder(bm));
    }
}
