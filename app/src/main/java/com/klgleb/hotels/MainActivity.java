/*
 313500 Luxury Hostel:
 https://dl.dropboxusercontent.com/u/109052005/1/313500.json -- нет отеля (404)

 13100 Best Western President Hotel at Times Square
 https://dl.dropboxusercontent.com/u/109052005/1/13100.json нет изображения (=null)

 85862 Dream (https://dl.dropboxusercontent.com/u/109052005/1/85862.json):
 https://dl.dropboxusercontent.com/u/109052005/1/a7.jpg - нет изображения, 404


 **/
package com.klgleb.hotels;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.klgleb.hotels.db.HelperFactory;
import com.klgleb.hotels.db.HotelDao;
import com.klgleb.hotels.rest.GetHotelsTask;
import com.klgleb.hotels.rest.Hotel;
import com.klgleb.hotels.rest.ITaskCallback;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.NonConfigurationInstance;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@EActivity(R.layout.activity_main)
@OptionsMenu(R.menu.menu_main)
public class MainActivity extends AppCompatActivity implements ITaskCallback<List<Hotel>> {
    public static final String TAG = "Hotels MainActivity";

    public static final int SORT_BY_DEFAULT = 0;
    public static final int SORT_BY_DISTANCE_DESC = 4;
    public static final int SORT_BY_DISTANCE = 3;
    public static final int SORT_BY_SUITES_AVAILABILTITY_DESC = 2;
    public static final int SORT_BY_SUITES_AVAILABILTITY = 1;

    @ViewById
    Toolbar toolbar;

    @ViewById(R.id.listView)
    ListView listView;


    @ViewById(R.id.activity_main_swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    @ViewById
    RelativeLayout notFoundLayout;

    @NonConfigurationInstance
    @Bean
    GetHotelsTask getHotelsTask;

    @NonConfigurationInstance
    ArrayList<Hotel> hotels;

    @NonConfigurationInstance
    Integer sorted = SORT_BY_DEFAULT;


    @AfterViews
    void afterViews() {
        setSupportActionBar(toolbar);
        notFoundLayout.setVisibility(View.GONE);

        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getHotels();
            }
        });

        setRefreshing(getHotelsTask.isLoading());

        loadFromCache();
    }


    @ItemClick(R.id.listView)
    void onItemClick(Hotel hotel) {
        ViewActivity_.intent(this).hotelId(hotel.getId()).start();
    }

    void getHotels() {

        Log.d(TAG, "loading data from server...");
        getHotelsTask.doRequest();
    }

    @Background
    void loadFromCache() {
        Log.d(TAG, "loading data from cache...");

        List<Hotel> allHotels = null;
        try {
            HotelDao hotelDao = HelperFactory.getHelper().getHotelDao();
            allHotels = hotelDao.getAllHotels();

        } catch (SQLException e) {
            Log.w(TAG, e);

        }

        if (allHotels != null && allHotels.size() == 0) {
            getHotels();
        } else {
            displayData(allHotels);
        }
    }


    @UiThread
    @Override
    public void setRefreshing(boolean refreshing) {
        swipeRefreshLayout.setRefreshing(refreshing);

    }

    @UiThread
    @Override
    public void onError(String s) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();

        if (this.hotels == null || this.hotels.size() == 0) {
            notFoundLayout.setVisibility(View.VISIBLE);
        }
    }

    @UiThread
    @Override
    public void displayData(List<Hotel> hotels) {
        if (hotels != null) {
            this.hotels = (ArrayList<Hotel>) hotels;
        }

        if (this.hotels != null) {
            sortCollections();

            MainAdapter adapter = new MainAdapter(this, this.hotels);
            listView.setAdapter(adapter);

            if (this.hotels.size() == 0) {
                notFoundLayout.setVisibility(View.VISIBLE);
            } else {
                notFoundLayout.setVisibility(View.INVISIBLE);
            }
        } else {
            notFoundLayout.setVisibility(View.VISIBLE);
        }
    }

    void sortCollections() {
        Collections.sort(hotels, new Comparator<Hotel>() {
            public int compare(Hotel o1, Hotel o2) {

                switch (sorted) {
                    case SORT_BY_DISTANCE:
                        return (o1.getDistance() - o2.getDistance()) >= 0 ? 1 : -1;

                    case SORT_BY_DISTANCE_DESC:
                        return (o2.getDistance() - o1.getDistance()) >= 0 ? 1 : -1;

                    case SORT_BY_SUITES_AVAILABILTITY:
                        return o1.getSuitesAvailability().length - o2.getSuitesAvailability().length;

                    case SORT_BY_SUITES_AVAILABILTITY_DESC:
                        return o2.getSuitesAvailability().length - o1.getSuitesAvailability().length;

                    case SORT_BY_DEFAULT:
                    default:
                        return 0;

                }
            }
        });
    }

    @OptionsItem(R.id.action_sort_distance)
    void sortDistance() {
        if (sorted == SORT_BY_DISTANCE) {
            sorted = SORT_BY_DISTANCE_DESC;
        } else {
            sorted = SORT_BY_DISTANCE;
        }

        displayData(null);

    }

    @OptionsItem(R.id.action_sort_suites_availability)
    void sortPagesNumber() {
        if (sorted == SORT_BY_SUITES_AVAILABILTITY) {
            sorted = SORT_BY_SUITES_AVAILABILTITY_DESC;
        } else {
            sorted = SORT_BY_SUITES_AVAILABILTITY;
        }

        displayData(null);
    }


}
